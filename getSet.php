<?php

  class Student {
    private $id = "Default";
    private $name;

    function __set($var, $value) {
      echo "Set method is not found, so this is being called.<br>";
      if ($var = 'name') {
        $this->name = $value;
      } elseif ($var = 'id') {
        $this->id = $value;
      }
    }

    function __get($var) {
      echo "Get methods is not found so this is called.<br>";
      if ($var = 'name') {
        echo "Name: " . $this->name . "<br>";
      } elseif ($var = 'id') {
        echo "ID: " . $this->id . "<br>";
      }
    }
  }

  $student1 = new Student();
  $student1->id = "Not-default";
  echo $student1->id . "<br>";
  $student1->name = "NameyNamey";
  echo $student1->name . "<br>";

?>
