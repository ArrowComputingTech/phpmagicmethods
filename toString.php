<?php
  //toString is called when object itself is printed/echoed. It can be used to print object properties.
  class Teacher {
    private $name = "Default name";
    function __toString() : string {
      echo "Name: {$this->name}<br>";
      return "To string method called.<br>";
    }
  }

  $teacher = new Teacher();
  echo $teacher . "<br>";
