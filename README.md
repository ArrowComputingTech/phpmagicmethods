  These php programs include descriptions and examples of some of php's magic methods.

    __set:
    invoked when trying to set protected or private properties.

    __get:
    invoked when trying to get protected or private properties.

    __call:
    Invoked when an undefined method is called.

    __toString:
    invoked when someone tries to print an object.

    __debuginfo:
    invoked when var_dump is used to print an object.
