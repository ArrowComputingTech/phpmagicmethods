<?php
  
  class Database {
    public $db = "SQL";
    public $dbname = "Students";
    public $tablename = "tbl_users";

    function hello() {
      return "Hello!<br>";
    }

    function __debugInfo() {
      echo "debugInfo called.<br>";
      echo "Name: {$this->name} ID: {$this->id}<br>";
    }
    function __toString() {
      return "Takes control for echo and print.<br>";
    }
  }

  $db = new Database();
  echo $db;
  print $db;
  var_dump($db);
?>
