<?php

  class User {
    private $name;
    private $id;

    function __get($var) {
      if ($var == 'name') {
        echo "__get called. Name: {$this->name}<br>";
      } elseif ($var = 'id') {
        echo "__get called. Id: {$this->id}<br>";
      } else {
        echo "__get called. Nonexistent variable passed.<br>";
      }
    }

    function __set($var, $val) {
      if ($var == 'name') {
        echo "__set called. Name set to {$val}<br>";
        $this->name = $val;
      } elseif ($var == 'id') {
        echo "__set called. ID set to {$val}<br>";
        $this->id = $val;
      }
    }

    function __call($func, $args) {
      echo "__call called. {$func} does not exist. Put your ";
      foreach ($args as $arg) {
        echo "$arg ";
      }
      echo "elsewhere.<br>";
    }

    function __toString() {
      echo "toString Name: {$this->name} toString ID: {$this->id}<br>";
      return "To string called.<br>";
    }

    function __debugInfo() {
      echo "debugInfo called. Name: {$this->name} debugInfo ID: {$this->id}<br>";
    }
  }
  $user1 = new User();
  echo $user1->name;
  $user1->name = "Fred";
  echo $user1->name;
  $user1->id = "789";
  echo $user1->id;
  $user1->doThing($invalidVar, $randomEntropy, $derpa);
  echo $user1;
  var_dump($user1);

?>


